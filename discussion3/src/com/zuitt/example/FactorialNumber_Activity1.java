package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber_Activity1 {

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed: ");

        try{
            int num = in.nextInt();
            int result = 1;

            if (num > 0){
                for(int i = num; i > 1; i--){
                    if(i == num){
                        result = i * (i-1);
                    } else{
                        result *= (i-1);
                    }
                }
                System.out.println("The factorial of " + num + " is " + result);
            } else if(num == 1){
                System.out.println("The factorial of " + num + " is " + result);
            } else if(num == 0){
                System.out.println("The factorial of " + num + " is " + result);
            } else{
                System.out.println("Input positive integer.");
            }
        } catch(Exception e){
            System.out.println("Invalid input. Please input a number");
        }




    }
}
