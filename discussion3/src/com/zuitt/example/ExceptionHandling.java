package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {

    public static void main(String[] args){

        // Exceptions
        // Refers to a problem that arises during the execution of the program; It disrupts the normal flow of the program and terminates it abnormally

        // Exception Handling
        // Refers to managing and catching run-time errors in order to safely run your code.
        // "Compile-time errors" are errors that usually happen when you try to compile a program that is syntactically incorrect or has a missing package.
        // "Run-time errors" are errors that happen after compilation and during the execution of the program

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number: ");

        // "try" code block - tries to do or execute the statement
        // "catch" code block - catches the error message
        // printStackTrace() - prints the throwable error along with the other details like the line number and class name where the exception occurred.
        try{
            num = input.nextInt();
        } catch(Exception e){
            System.out.println("Invalid input. Please enter a number.");
            e.printStackTrace();
        }

        // Option Block
        // "finally block" is a particularly useful when dealing with highly sensitive operation, such as:
        // - Ensuring safe operation on database by closing them if they are no longer needed.
        // - Operations that have unpredictable situations such as if it is possible for users to input problematic information
        // - Any operations that may be susceptible to abuse and malicious acts, such as downloading or saving files.

        finally{
            System.out.println("You have entered: " + num);
        }

        num = input.nextInt();
        System.out.println("Hello World");

    }
}
